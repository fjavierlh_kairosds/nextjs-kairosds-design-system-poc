import { KdsButton, KdsHeader } from "@kairosds/components-react";
import React from "react";

const KairosComponents = () => {
  return (
    <div>
      <KdsHeader logo="https://dummyimage.com/100x50/000/fff&text=Logo" />
      <KdsButton text="I'm a button!" />
    </div>
  );
};

export default KairosComponents;
