const withTM = require("next-transpile-modules")([
  "@kairosds/components-react",
]); // pass the modules you would like to see transpiled
module.exports = withTM({
  assetPrefix: ".",
});
